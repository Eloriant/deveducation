using NUnit.Framework;

namespace HomeWorks.Tests
{

    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CalcCreditPayments_amount1000_years2_percent10_result200x1100()
        {
            //arrange
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int amount = 1000, years = 2;
            double percent = 20;

            double[] expected = new double[] { 1707, 40976 };

            //act

            double[] actual = homeWork_1.CalcCreditPayments(amount, years, percent);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void GetLengthHypotenuse_firstCathet10_secondCathet15_expected18()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int firstCathet = 10, secondCathet = 15;
            double expected = 18;

            double actual = homeWork_1.GetLengthHypotenuse(firstCathet, secondCathet);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetEquation_firstX5_firstY7_secondX9_secondX3_k5_b7()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int firstX = 5, firstY = 7, secondX = 9, secondY = 3;

            double[] expected = new double[] { -1, 12 };

            double[] actual = homeWork_1.GetEquation(firstX, firstY, secondX, secondY);

            Assert.AreEqual(expected, actual);
        }

        [Test]

        public void GetRoots_a3_b4_c1_expected0x1()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            double a = 3, b = 4, c = 1;

            string expected = "x1 = -0,3 x2 = -1";

            string actual = homeWork_1.GetRoots(a, b, c);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetExpression_firstNum10_secondNum5_thirdNum13_expected653()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int firstNum = 10, secondNum = 5, thirdNum = 13;

            int expected = 653;

            int actual = homeWork_1.GetExpression(firstNum, secondNum, thirdNum);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void DivOrNot_firstNum11_secondNum5_expectedStr()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int firstNum = 11, secondNum = 5;

            string expected = "Residual = 1 Result = 2";

            string actual = homeWork_1.DivOrNot(firstNum, secondNum);

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void GetQuarterPoint_x5_y7_expected1()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int x = 5, y = 7;

            int expected = 1;

            int actual = homeWork_1.GetQuarterPoint(x, y);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void PointInCyrcle_x5_y11_r12_expected0()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int x = 5, y = 11, r = 12;

            int expected = 0;

            int actual = homeWork_1.PointInCyrcle(x, y, r);

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void getFactorial_number15_expected20() {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int number = 15;

            long expected = 1307674368000;

            long actual = homeWork_1.getFactorial(15);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetSumAndMulti_number375_expected20x100()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int number = 375;

            long[] expected = new long[] { 15, 105 };

            long[] actual = homeWork_1.GetSumAndMulti(number);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetProof_number30_expected1()
        {
            HomeWork_1 homeWork_1 = new HomeWork_1();
            int number = 30;

            long expected = 1;

            long actual = homeWork_1.GetProof(number);

            Assert.AreEqual(expected, actual);
        }
    }
}
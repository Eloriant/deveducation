﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HomeWorks
{
    public class HomeWork_2
    {
        #region Task_1

        public int GetReverseNumber(int number)

        {
            return int.Parse(number.ToString().ToCharArray().Reverse().ToArray());
        }

        #endregion



        #region Task_2

        public double Calculate(int number1, char op, int number2)

        {

            double result = 0;

            switch (op)

            {

                case '+':

                    result = Addition(number1, number2);

                    break;

                case '-':

                    result = Subtraction(number1, number2);

                    break;



                case '*':

                    result = Multi(number1, number2);

                    break;



                case '/':

                    result = Div(number1, number2);

                    break;



                default:

                    WriteExep("Вы ввели не операции");

                    break;

            }

            return result;

        }



        public void WriteExep(string exeption)

        {

            Console.WriteLine(exeption);

        }

        public int Multi(int number1, int number2)

        {

            return number1 * number2;

        }



        public double Div(int number1, int number2)

        {

            return number1 / number2;

        }



        public int Addition(int number1, int number2)

        {

            return number1 + number2;

        }



        public int Subtraction(int number1, int number2)

        {

            return number1 - number2;

        }

        #endregion



        #region Task_3

        public string TestRandom()

        {

            Random rand = new Random();



            int even = 0;

            for (int i = 0; i < 1000; i++)

            {

                even += rand.Next(1, 1000) % 2 == 0 ? 1 : 0;

            }

            return $"Even = {even} \n Odd = {1000 - even}";

        }



        #endregion



        #region Task_4

        public void GuessTheNumber(int estimatedNum)

        {

            Random rand = new Random();

            int number = rand.Next(1, 100);



            for (int i = 0; i < 9; i++)

            {

                if (estimatedNum > number)

                {

                    WriteMessage("Вы ввели число больше загаданного");

                }

                else if (estimatedNum < number)

                {

                    WriteMessage("Вы ввели число меньше загаданного");

                }

                if (estimatedNum == number) { WriteMessage($"Поздравляем, вы угадали число {number}"); break; }

                WriteMessage($"Осталось попыток: {9 - i} \nВведите предполагаемое число");

                estimatedNum = InputNum();

                if (i == 8) { WriteMessage($"Вы не угадали число за 10 попыток. \nЧисло: {number}"); }



                Console.WriteLine($"{(estimatedNum > number ? "+" : "-")}");

            }



        }



        public void WriteMessage(string message)

        {

            Console.WriteLine(message);

        }



        public int InputNum()

        {

            return int.Parse(Console.ReadLine());

        }



        #endregion



        #region Task_5

        public string GetFibRange(int number)

        {

            string result = "";

            int one = 0, two = 1;

            for (int i = 0; i < number; i++)

            {

                one = two + one;

                two = one - two;

                result += $"{one} ";

            }

            return result;

        }



        #endregion



        #region Task_6

        public int GetMax(int[] mass)

        {

            int max = mass[0];

            for (int i = 1; i < mass.Length; i++)

            {

                max = max < mass[i] ? mass[i] : max;

            }

            return max;

        }



        #endregion



        #region Task_7

        public int[] MassReverse(int[] array)

        {

            int lastInd = array.Length - 1;

            for (int i = 0; i < array.Length / 2; i++)

            {

                int tmp = array[i];

                array[i] = array[lastInd];

                array[lastInd] = tmp;

                lastInd--;

            }



            return array;

        }



        #endregion



        #region Task_8

        public int[] SwitchMass(int[] array)

        {

            int masLen = array.Length / 2;

            int cPos = masLen + array.Length % 2;

            for (int i = 0; i < masLen; i++)

            {

                int temp = array[i];

                array[i] = array[cPos + i];

                array[cPos + i] = temp;

            }

            return array;

        }



        #endregion



        #region Task_9


        public int[] GetNumberLessAverage(int[] array)
        {
            double masAverage = array.Average();
            List<int> mas = new List<int>();

            foreach(int tmp in array)
            {
                if(tmp < masAverage)
                {
                    mas.Add(tmp);
                }
            }

            return mas.ToArray();
        }

        public double GetAverage(int[] array)
        {
            double allArray = 0;
            for(int i = 0; i < array.Length; i++)
            {
                allArray += array[i];
            }

            return allArray / array.Length;
        }

        #endregion



        #region Task_10

        public int GetSummBetweenMinAndMax(int min, int max)

        {

            int[] mas = new int[10];

            int imin = 0;

            int imax = 0;

            int sum = 0;



            for (int i = 0; i < 10; i++)

            {

                if (mas[i] > mas[imax]) imax = i;

                if (mas[i] < mas[imin]) imin = i;

            }

            int change;

            if (imin > imax)

            {

                change = imin;

                imin = imax;

                imax = change;

            }

            for (int i = imin + 1; i < imax; i++)

            {

                sum += mas[i];

            }



            return sum;

        }



        #endregion
    }
}

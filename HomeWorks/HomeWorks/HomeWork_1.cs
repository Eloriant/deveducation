﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWorks
{
    public class HomeWork_1
    {
        #region Task_1
        public double[] CalcCreditPayments(int amountCredit, int years, double percent)
        {
            double monthlyPay = getMonthlyPay(amountCredit, years, percent);

            
            //double[] result = new double[2];
            //result[0] = monthlyPay;
            //result[1] = getTotalPayout(monthlyPay, years);
            return new double[] { Math.Round(monthlyPay,0), Math.Round(getTotalPayout(monthlyPay, years),0) } ;
        }

        private double getMonthlyPay(int amountCredit, int years, double percent) => (amountCredit * percent * (1 + percent) * years) /
                                                                               (12 * ((1 + percent) * years - 1));

        private double getTotalPayout(double monthlyPay, int years) => (monthlyPay * 12) * years;

        #endregion

        #region Task_2

        public double GetLengthHypotenuse(double firstCathet, double secondCathet)
        {
            return Math.Round(Math.Sqrt(firstCathet * firstCathet + secondCathet * secondCathet),0);
        }

        #endregion

        #region Task_3
        public double[] GetEquation(int firstX, int firstY, int secondX, int secondY)
        {
            Point firstPoint = new Point(firstX, firstY);

            Point secondPoint = new Point(secondX, secondY);

            return getEquation(firstPoint, secondPoint);
        }
        private double[] getEquation(Point y, Point x)
        {
            double k = (x.pointY - y.pointY) / (x.pointX - y.pointX);
            double b = (x.pointX * y.pointY - y.pointX * x.pointY) / (x.pointX - y.pointX);

            return new double[] { k, b };
        }

        class Point
        {
            public Point(double pointX, double pointY)
            {
                this.pointX = pointX;
                this.pointY = pointY;
            }
            public double pointX { get; set; }
            public double pointY { get; set; }
        }
        #endregion

        #region Task_4
        public string GetRoots(double a, double b, double c)
        {
            double  D, x1, x2;

            string result;

            D = Math.Pow(b, 2) - 4 * a * c;
            if (D >= 0)
            {
                x1 = (-b + Math.Sqrt(D)) / (2 * a);
                x2 = (-b - Math.Sqrt(D)) / (2 * a);

                result = $"x1 = {Math.Round(x1,1)} x2 = {x2}";
            }
            else
            {
                result = "No real roots";
            }

            return result;
        }
        #endregion

        #region Task_5
        public int GetExpression(int firstNum, int secondNum, int thirdNum)
        {
            int result = Multi(firstNum, secondNum, thirdNum) > Addition(firstNum, secondNum, thirdNum) ? 
                Multi(firstNum, secondNum, thirdNum) + 3 : Addition(firstNum, secondNum, thirdNum) + 3;
            return result;
        }
        private int Addition(int a, int b, int c)
        {
            return a + b + c;
        }

        private int Multi(int a, int b, int c)
        {
            return a * b * c;
        }
        #endregion

        #region Task_6

        public string DivOrNot(int number1, int number2)
        {
            return GetResidual(number1, number2);
        }
        private string GetResidual(int number1, int number2)
        {
            int residual = number1 % number2;
            int result = number1 / number2;

            return residual == 0 ? $"{number1}/{number2} = {result}" :
                                        $"Residual = {residual} Result = {result}";

        }
        #endregion

        #region Task_7
        public int GetQuarterPoint(int x, int y)
        {
            int result = 0;
            if (x == 0 || y == 0) { result = 0; }
            else
            {
                if (x > 0 && y > 0) { result = 1; }
                else if (x < 0 && y > 0) { result = 2; }
                else if (x < 0 && y < 0) { result = 3; }
                else if (x > 0 && y < 0) { result = 4; }
            }

            return result;
        }
        #endregion

        #region Task_8

        public int PointInCyrcle(int x, int y, int r)
        {
            return Math.Sqrt(x * x + y * y) > r ? 0 : 1;
        }
        #endregion

        #region Task_9
        public long getFactorial(int number)
        {
            long result = 1;

            for (int i = 1; i <= number; i++)
            {
                result *= i;
            }

            return result;
        }
        #endregion

        #region Task_10
        public StringBuilder GetFunctionResult()
        {
            double x1, x2, step, res;
            StringBuilder result = new StringBuilder();

            Random rand = new Random();

            x1 = rand.Next(-10, 15);
            x2 = rand.Next(-10, 15);
            step = rand.Next(0, 5);

            Console.WriteLine($"min = {x1}");
            Console.WriteLine($"max = {x2}");
            Console.WriteLine($"step = {step}");
            while (x1 < x2)
            {
                res = -0.23 * x1 * x1 + x1;
                result.Append($"{x1} --> {res}");
                x1 += step;
            }
            return result;
        }
        #endregion

        #region Task_11
        public long[] GetSumAndMulti(int num)
        {

            long sum = 0;
            double multi = 1;

            while (num != 0)
            {
                sum += num % 10;
                multi *= num % 10;
                num /= 10;
            }
            return new long[] { sum, Convert.ToInt64(multi) };
        }
        #endregion

        #region Task_12
        public int GetProof(int number)
        {

            int arithmeticProgressionSum = GetArithmeticProgressionSum(number);

            return arithmeticProgressionSum == number * (number + 1) / 2 ? 1 : 0;
        }
        static int GetArithmeticProgressionSum(int num)
        {
            int factorial = 0;
            for (int i = 1; i <= num; i++)
            {
                factorial += i;
            }
            return factorial;
        }
        #endregion
    }
}
